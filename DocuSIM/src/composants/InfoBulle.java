/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package composants;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import application.Theme;
import verification.Erreur;
import verification.Probleme;
import verification.Severite;

/**
 * Info-bulle qui affiche les problèmes dans le code.
 * 
 * @author Jean-Félix Morency
 */
public class InfoBulle extends JPanel {

	/** Identifiant de sérialisation. */
	private static final long serialVersionUID = 1L;

	/** La largeur du conteneur. */
	private final static int LARGEUR_CONTENEUR = 1000;

	/** Le problème affiché. */
	private Probleme probleme;
	/** Le panneau de l'info-bulle. */
	private JPanel panelInfoBulle;

	/**
	 * Crée une info-bulle.
	 */
	public InfoBulle() {
		setOpaque(false);

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		panelInfoBulle = new JPanel();
		panelInfoBulle
				.setBorder(new CompoundBorder(new LineBorder(new Color(100, 100, 100)), new EmptyBorder(2, 0, 4, 0)));
		Theme.ajouterPanneau(panelInfoBulle);
		GridBagConstraints gbcPanelInfoBulle = new GridBagConstraints();
		gbcPanelInfoBulle.anchor = GridBagConstraints.SOUTHWEST;
		gbcPanelInfoBulle.gridx = 0;
		gbcPanelInfoBulle.gridy = 0;
		add(panelInfoBulle, gbcPanelInfoBulle);
		panelInfoBulle.setLayout(new BoxLayout(panelInfoBulle, BoxLayout.Y_AXIS));

		Theme.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				probleme = null;
			}
		});
	}

	/**
	 * Met à jour le contenu de l'info-bulle.
	 */
	private void changerContenu() {
		panelInfoBulle.removeAll();
		for (Erreur erreur : probleme.getErreursActives()) {
			ajouterEtiquette(erreur);
		}
		revalidate();
	}

	/**
	 * Ajoute une étiquette dans l'info-bulle.
	 * 
	 * @param erreur l'erreur.
	 */
	private void ajouterEtiquette(Erreur erreur) {
		JPanel panelErreur = new JPanel();
		panelErreur.setLayout(new BorderLayout(5, 0));
		panelErreur.setBorder(new EmptyBorder(1, 5, 1, 5));
		panelErreur.setOpaque(false);
		panelInfoBulle.add(panelErreur);

		JLabel lblSymbole = new JLabel(erreur.getSeverite() == Severite.AVERTISSEMENT ? "!" : "×");
		lblSymbole.setFont(new Font("Tahoma", Font.BOLD, erreur.getSeverite() == Severite.AVERTISSEMENT ? 11 : 12));
		lblSymbole.setForeground(Theme.getCouleurErreur(erreur.getSeverite()));
		lblSymbole.setPreferredSize(new Dimension(10, 14));
		lblSymbole.setHorizontalAlignment(SwingConstants.CENTER);
		panelErreur.add(lblSymbole, BorderLayout.WEST);

		JLabel lblMessage = new JLabel(erreur.getDescription());
		lblMessage.setForeground(Theme.getCouleurTexte());
		panelErreur.add(lblMessage, BorderLayout.CENTER);
	}

	/**
	 * Modifie la position de l'info-bulle.
	 * 
	 * @param x la coordonée x de l'info-bulle.
	 * @param y la coordonée y de l'info-bulle.
	 */
	public void setPosition(int x, int y) {
		setBounds(x, y - LARGEUR_CONTENEUR, LARGEUR_CONTENEUR, LARGEUR_CONTENEUR);
	}

	/**
	 * Modifie le problème affiché.
	 * 
	 * @param probleme le problème affiché.
	 */
	public void setProbleme(Probleme probleme) {
		this.probleme = probleme;
		if (this.probleme != null)
			changerContenu();
	}

	/**
	 * Retourne le problème affiché.
	 * 
	 * @return le problème affiché.
	 */
	public Probleme getProbleme() {
		return probleme;
	}

}
