/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package composants;

import java.awt.Graphics;

import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

import application.Theme;

/**
 * Interface d'un panneau séparé synchronisé avec le thème.
 * 
 * @author Jean-Félix Morency
 */
public class InterfaceSplitPane extends BasicSplitPaneUI {

	/**
	 * Crée un diviseur pour le panneau.
	 * 
	 * @return le diviseur.
	 */
	@Override
	public BasicSplitPaneDivider createDefaultDivider() {
		return new Diviseur(this);
	}

	/**
	 * Diviseur pour le panneau.
	 * 
	 * @author Jean-Félix Morency
	 */
	private static class Diviseur extends BasicSplitPaneDivider {
		/** Identifiant de sérialisation. */
		private static final long serialVersionUID = 1L;

		/**
		 * Crée un diviseur pour le panneau.
		 * 
		 * @param ui l'interface du panneau.
		 */
		public Diviseur(BasicSplitPaneUI ui) {
			super(ui);
		}

		/**
		 * Dessine le diviseur.
		 * 
		 * @param g le contexte graphique.
		 */
		@Override
		public void paint(Graphics g) {
			g.setColor(Theme.getCouleurPanneau());
			g.fillRect(0, 0, getWidth(), getHeight());
		}
	}

}
