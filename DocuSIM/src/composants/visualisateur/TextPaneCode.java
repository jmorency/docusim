/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package composants.visualisateur;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import com.github.javaparser.JavaToken;
import com.github.javaparser.Position;
import com.github.javaparser.Range;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.EnumConstantDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.VariableDeclarationExpr;
import com.github.javaparser.resolution.declarations.ResolvedValueDeclaration;

import application.Theme;
import verification.ElementSyntaxique;
import verification.Severite;

/**
 * Composant textuel qui permet de visualiser du code source.
 * 
 * @author Jean-Félix Morency
 */
public class TextPaneCode extends JTextPane {

	/** Identifiant de sérialisation. */
	private static final long serialVersionUID = 1L;

	/** La position du début de chaque ligne. */
	private List<Integer> debutLignes = new ArrayList<>();
	/** Les lignes qui sont surlignées. */
	private Map<Integer, Severite> lignesSurlignees = new HashMap<>();

	/**
	 * Crée un visualisateur de code.
	 */
	public TextPaneCode() {
		super();
		setEditable(false);
		setOpaque(false);
		setFont(new Font("Consolas", Font.PLAIN, 13));
		setDisabledTextColor(Color.GRAY);
	}

	/**
	 * Permet d'afficher une barre de défilement horizontale lorsque le texte du
	 * panneau est plus large que le conteneur.
	 * 
	 * @return vrai si le texte est plus large que le conteneur.
	 */
	@Override
	public boolean getScrollableTracksViewportWidth() {
		return getUI().getPreferredSize(this).width <= getParent().getSize().width;
	}

	/**
	 * Dessine le composant.
	 * 
	 * @param g le contexte graphique.
	 */
	@Override
	protected void paintComponent(Graphics g) {
		g.setColor(getBackground());
		g.fillRect(0, 0, getWidth(), getHeight());

		try {
			for (Entry<Integer, Severite> ligne : lignesSurlignees.entrySet()) {
				g.setColor(Theme.getCouleurSurligneur(ligne.getValue()));
				Rectangle2D rect = modelToView2D(debutLignes.get(ligne.getKey() - 1));
				g.fillRect(0, (int) rect.getY(), getWidth(), (int) rect.getHeight());
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}

		super.paintComponent(g);
	}

	/**
	 * Retourne la position d'une ligne.
	 * 
	 * @param ligne la ligne.
	 */
	public void defilerALigne(int ligne) {
		try {
			int lignesDemiHauteur = (int) (getParent().getHeight() / (2 * modelToView2D(0).getHeight()));
			if (getCaretPosition() > debutLignes.get(ligne)) {
				setCaretPosition(debutLignes.get(Math.max(ligne - 1 - lignesDemiHauteur, 0)));
			} else {
				setCaretPosition(debutLignes.get(Math.min(ligne - 1 + lignesDemiHauteur, debutLignes.size() - 1)));
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Surligne une ligne.
	 * 
	 * @param ligne    la ligne à surligner.
	 * @param severite la sévérité pour laquelle surligner.
	 */
	public void surlignerLigne(int ligne, Severite severite) {
		if (lignesSurlignees.containsKey(ligne)) {
			List<Severite> severites = new ArrayList<>();
			severites.add(severite);
			severites.add(lignesSurlignees.get(ligne));
			lignesSurlignees.put(ligne, Severite.maximale(severites));
		} else {
			lignesSurlignees.put(ligne, severite);
		}
		repaint();
	}

	/**
	 * Efface le surlignement des lignes.
	 */
	public void effacerSurligneur() {
		lignesSurlignees.clear();
	}

	/**
	 * Remplace le texte dans le panneau.
	 * 
	 * @param texte le nouveau texte.
	 */
	@Override
	public void setText(String texte) {
		lignesSurlignees = new HashMap<>();
		debutLignes = new ArrayList<>();
		int debut = 0;
		do {
			debutLignes.add(debut);
			debut = texte.indexOf('\n', debut) + 1;
		} while (debut > 0);

		StyledDocument document = getStyledDocument();
		try {
			document.remove(0, document.getLength());
			document.insertString(0, texte, null);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Retourne le nombre de lignes.
	 * 
	 * @return le nombre de lignes.
	 */
	public int getNbLignes() {
		return debutLignes.size();
	}

	/**
	 * Remplace le code affiché dans le panneau.
	 * 
	 * @param code             le code.
	 * @param uniteCompilation l'unité de compilation associée au code.
	 */
	public void setCode(String code, CompilationUnit uniteCompilation) {
		setText(code);
		colorationSyntaxique(uniteCompilation);
	}

	/**
	 * Souligne une plage de caractères avec un trait ondulé.
	 * 
	 * @param plage   la plage de caractères.
	 * @param couleur la couleur du trait.
	 * @throws BadLocationException si la plage est invalide.
	 */
	public void souligner(Range plage, Color couleur) throws BadLocationException {
		getHighlighter().addHighlight(indice(plage.begin), indice(plage.end), new SurligneurOndule(couleur));
	}

	/**
	 * Retourne l'indice associé à une position.
	 * 
	 * @param position la position.
	 * @return l'indice.
	 */
	private int indice(Position position) {
		return debutLignes.get(position.line - 1) + position.column - 1;
	}

	/**
	 * Retourne la position associé à un point.
	 * 
	 * @param point le point.
	 * @return la position.
	 */
	public Position getPosition(Point2D point) {
		int pos = viewToModel2D(point);
		int ligne = debutLignes.size() - 1;
		while (debutLignes.get(ligne) > pos) {
			ligne--;
		}
		return new Position(ligne + 1, pos - debutLignes.get(ligne) + 1);
	}

	/**
	 * Colore un élément dans le panneau.
	 * 
	 * @param plage   la plage de l'élément.
	 * @param element le type de l'élément.
	 */
	private void colorer(Range plage, ElementSyntaxique element) {
		int debut = indice(plage.begin);
		int fin = indice(plage.end);
		SimpleAttributeSet attributs = new SimpleAttributeSet();
		StyleConstants.setForeground(attributs, Theme.getCouleurElementSyntaxique(element));
		getStyledDocument().setCharacterAttributes(debut, fin - debut + 1, attributs, false);
	}

	/**
	 * Colore un noeud dans le panneau.
	 * 
	 * @param noeud   le noeud.
	 * @param element le type de noeud.
	 */
	private void colorer(Node noeud, ElementSyntaxique element) {
		noeud.getRange().ifPresent(plage -> colorer(plage, element));
	}

	/**
	 * Colore un jeton dans le panneau.
	 * 
	 * @param jeton   le jeton.
	 * @param element le type du jeton.
	 */
	private void colorer(JavaToken jeton, ElementSyntaxique element) {
		jeton.getRange().ifPresent(plage -> colorer(plage, element));
	}

	/**
	 * Applique la coloration syntaxique au code.
	 * 
	 * @param uniteCompilation l'unité de compilation.
	 */
	public void colorationSyntaxique(CompilationUnit uniteCompilation) {
		// Mots-clés
		uniteCompilation.getTokenRange().ifPresent(jetons -> jetons.forEach(jeton -> {
			if (jeton.getCategory() == JavaToken.Category.KEYWORD)
				colorer(jeton, ElementSyntaxique.MOT_CLE);
		}));

		// Commentaires
		uniteCompilation.getAllComments().forEach(commentaire -> {
			if (commentaire.isJavadocComment()) {
				colorer(commentaire, ElementSyntaxique.JAVADOC);
			} else {
				colorer(commentaire, ElementSyntaxique.COMMENTAIRE);
			}
		});

		// Les autres éléments
		uniteCompilation.walk(noeud -> {
			switch (noeud.getClass().getSimpleName()) {
			// Expressions littérales
			case "NullLiteralExpr":
			case "BooleanLiteralExpr":
				colorer(noeud, ElementSyntaxique.MOT_CLE);
				break;

			case "IntegerLiteralExpr":
			case "LongLiteralExpr":
			case "DoubleLiteralExpr":
				colorer(noeud, ElementSyntaxique.NOMBRE);
				break;

			case "StringLiteralExpr":
				colorer(noeud, ElementSyntaxique.CHAINE);
				break;

			// Classes
			case "ClassOrInterfaceDeclaration":
				colorer(((ClassOrInterfaceDeclaration) noeud).getNameAsExpression(), ElementSyntaxique.CLASSE);
				break;

			case "ClassOrInterfaceType":
				noeud.getTokenRange().ifPresent(jetons -> jetons.forEach(jeton -> {
					if (jeton.getCategory() == JavaToken.Category.IDENTIFIER)
						colorer(jeton, ElementSyntaxique.CLASSE);
				}));
				break;

			// Méthodes
			case "ConstructorDeclaration":
				ConstructorDeclaration cd = (ConstructorDeclaration) noeud;
				colorer(cd.getNameAsExpression(), ElementSyntaxique.METHODE);
				cd.getAnnotations().forEach(annotation -> colorer(annotation, ElementSyntaxique.ANNOTATION));
				cd.getParameters().forEach(param -> colorer(param.getNameAsExpression(), ElementSyntaxique.PARAMETRE));
				break;

			case "MethodDeclaration":
				MethodDeclaration md = (MethodDeclaration) noeud;
				colorer(md.getNameAsExpression(), ElementSyntaxique.METHODE);
				md.getAnnotations().forEach(annotation -> colorer(annotation, ElementSyntaxique.ANNOTATION));
				md.getParameters().forEach(param -> colorer(param.getNameAsExpression(), ElementSyntaxique.PARAMETRE));
				break;

			case "MethodCallExpr":
				colorer(((MethodCallExpr) noeud).getNameAsExpression(), ElementSyntaxique.METHODE);
				break;

			// Variables et propriétés
			case "VariableDeclarationExpr":
				((VariableDeclarationExpr) noeud).getVariables()
						.forEach(variable -> colorer(variable.getNameAsExpression(), ElementSyntaxique.VARIABLE));
				break;

			case "FieldDeclaration":
				((FieldDeclaration) noeud).getVariables()
						.forEach(variable -> colorer(variable.getNameAsExpression(), ElementSyntaxique.PROPRIETE));
				break;

			case "EnumConstantDeclaration":
				colorer(((EnumConstantDeclaration) noeud).getNameAsExpression(), ElementSyntaxique.PROPRIETE);
				break;

			case "FieldAccessExpr":
				colorer(((FieldAccessExpr) noeud).getNameAsExpression(), ElementSyntaxique.PROPRIETE);
				break;

			// Les autres symboles
			case "NameExpr":
				try {
					ResolvedValueDeclaration declaration = ((NameExpr) noeud).resolve();
					if (declaration.isField() || declaration.isEnumConstant()) {
						colorer(noeud, ElementSyntaxique.PROPRIETE);
					} else if (declaration.isParameter()) {
						colorer(noeud, ElementSyntaxique.PARAMETRE);
					} else if (declaration.isVariable()) {
						colorer(noeud, ElementSyntaxique.VARIABLE);
					}
				} catch (Exception e) {
					colorer(noeud, ElementSyntaxique.CLASSE);
				}
				break;
			}
		});
	}

}
