/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package composants.arbre;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;

import com.github.javaparser.Range;

import application.Theme;
import verification.Erreur;

/**
 * Afficheur de cellule pour l'arbre des erreurs.
 * 
 * @author Jean-Félix Morency
 */
public class AfficheurCelluleErreur extends AfficheurCellule {

	/** Identifiant de sérialisation. */
	private static final long serialVersionUID = 1L;

	/** Le conteneur de l'arbre. */
	private JViewport conteneur;

	/**
	 * Retourne le composant à afficher pour une cellule.
	 * 
	 * @param tree     l'arbre.
	 * @param value    la cellule.
	 * @param sel      si la cellule est sélectionnée.
	 * @param expanded si la cellule est étendue.
	 * @param leaf     si la cellule est une feuille.
	 * @param row      le numéro de rangée de la cellule.
	 * @param hasFocus si la cellule a le focus.
	 * @return le composant à afficher pour une cellule.
	 */
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf,
			int row, boolean hasFocus) {

		if (tree.getParent() != null) {
			this.conteneur = (JViewport) tree.getParent();
		}
		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
		if (node.getUserObject() == null || node.getUserObject() instanceof String) {
			return this;
		}

		Object[] erreur = (Object[]) node.getUserObject();
		setForeground(Theme.getCouleurTexte().darker());

		if (erreur.length == 2) {
			setIcon(UIManager.getIcon("Tree.leafIcon"));
			setBackground(Theme.getCouleurFond());
			setText((String) erreur[1]);
		} else if (node.getUserObject() != null) {
			setIcon(null);
			Erreur err = (Erreur) erreur[1];
			setBackground(Theme.getCouleurSurligneur(err.getSeverite()));
			setText(String.format("%s (ligne %d)", err.getTitre(), ((Range) erreur[2]).begin.line));
		}

		return this;
	}

	/**
	 * Dessine le composant à afficher pour une cellule.
	 * 
	 * @param g le contexte graphique.
	 */
	@Override
	protected void paintComponent(Graphics g) {
		Point point = new Point();
		SwingUtilities.convertPointToScreen(point, conteneur);
		SwingUtilities.convertPointFromScreen(point, this);

		g.setColor(getBackground());
		g.setClip((int) point.getX(), (int) point.getY(), conteneur.getWidth(), conteneur.getHeight());
		g.fillRect((int) point.getX(), 0, conteneur.getWidth(), getHeight());
		super.paintComponent(g);
	}

}
