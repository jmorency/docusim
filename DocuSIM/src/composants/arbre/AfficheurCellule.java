/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package composants.arbre;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import application.Theme;

/**
 * Afficheur de cellule d'arbre synchronisé avec le thème.
 * 
 * @author Jean-Félix Morency
 */
public class AfficheurCellule extends DefaultTreeCellRenderer {

	/** Identifiant de sérialisation. */
	private static final long serialVersionUID = 1L;

	/**
	 * Retourne la couleur de fond pour une cellule sélectionnée.
	 * 
	 * @return la couleur de fond pour une cellule sélectionnée.
	 */
	@Override
	public Color getBackgroundSelectionColor() {
		return Theme.isSombre() ? Color.DARK_GRAY : new Color(220, 220, 255);
	}

	/**
	 * Retourne la couleur de fond pour une cellule non sélectionnée.
	 * 
	 * @return la couleur de fond pour une cellule non sélectionnée.
	 */
	@Override
	public Color getBackgroundNonSelectionColor() {
		return Theme.getCouleurFond();
	}

	/**
	 * Retourne le composant à afficher pour une cellule.
	 * 
	 * @param tree     l'arbre.
	 * @param value    la cellule.
	 * @param sel      si la cellule est sélectionnée.
	 * @param expanded si la cellule est étendue.
	 * @param leaf     si la cellule est une feuille.
	 * @param row      le numéro de rangée de la cellule.
	 * @param hasFocus si la cellule a le focus.
	 * @return le composant à afficher pour une cellule.
	 */
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf,
			int row, boolean hasFocus) {

		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
		setForeground(Theme.getCouleurTexte());

		return this;
	}
}
