/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package verification;

/**
 * Enumération des types d'éléments syntaxiques.
 * 
 * @author Jean-Félix Morency
 */
public enum ElementSyntaxique {
	/** Un mot-clé. */
	MOT_CLE,
	/** Un commentaire. */
	COMMENTAIRE,
	/** Un commentaire Javadoc. */
	JAVADOC,
	/** Une classe. */
	CLASSE,
	/** Une annotation. */
	ANNOTATION,
	/** Une méthode. */
	METHODE,
	/** Une propriété. */
	PROPRIETE,
	/** Un paramètre. */
	PARAMETRE,
	/** Une variable. */
	VARIABLE,
	/** Un nombre. */
	NOMBRE,
	/** Une chaîne de caractères. */
	CHAINE,
}
