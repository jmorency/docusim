/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package verification;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.github.javaparser.Range;
import com.github.javaparser.ast.Node;

/**
 * Représente un problème de documentation.
 * 
 * @author Jean-Félix Morency
 */
public class Probleme {

	/** La plage du problème. */
	private Range plage;
	/** Les erreurs liées au problème. */
	private List<Erreur> erreurs;

	/**
	 * Crée un nouveau problème.
	 * 
	 * @param noeud   le noeud du problème.
	 * @param erreurs les erreurs liées au problème.
	 */
	public Probleme(Node noeud, List<Erreur> erreurs) {
		this.plage = noeud.getRange().get();
		this.erreurs = erreurs;
	}

	/**
	 * Crée un nouveau problème.
	 * 
	 * @param noeud   la noeud qui a le problème.
	 * @param erreurs les erreurs liées au problème.
	 */
	public Probleme(Node noeud, Erreur... erreurs) {
		this(noeud, Arrays.asList(erreurs));
	}

	/**
	 * Crée un nouveau problème.
	 * 
	 * @param plage   la plage du probléme.
	 * @param erreurs les erreurs liées au problème.
	 */
	public Probleme(Range plage, Erreur... erreurs) {
		this.plage = plage;
		this.erreurs = Arrays.asList(erreurs);
	}

	/**
	 * Retourne la sévérité du problème.
	 * 
	 * @return la sévérité du problème.
	 */
	public Severite getSeverite() {
		Set<Severite> severites = new HashSet<>();
		for (Erreur erreur : getErreursActives()) {
			severites.add(erreur.getSeverite());
		}
		return Severite.maximale(severites);
	}

	/**
	 * Retourne la plage du problème.
	 * 
	 * @return la plage du problème.
	 */
	public Range getPlage() {
		return plage;
	}

	/**
	 * Retourne les erreurs liées au problème.
	 * 
	 * @return les erreurs liées au problème.
	 */
	public List<Erreur> getErreurs() {
		return erreurs;
	}

	/**
	 * Retourne les erreurs actives liées au problème.
	 * 
	 * @return les erreurs actives liées au problème.
	 */
	public List<Erreur> getErreursActives() {
		return erreurs.stream().filter(erreur -> erreur.isActive()).toList();
	}

}
