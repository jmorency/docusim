/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package verification;

import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

/**
 * Décrit une erreur de documentation.
 * 
 * @author Jean-Félix Morency
 */
public class Erreur {

	/** Les préférences de l'utilisateur. */
	private static Preferences preferences = Preferences.userRoot().node("docusim").node("erreurs");
	/** L'ensemble des erreurs. */
	private static List<Erreur> erreurs = new ArrayList<>();

	/** L'identifiant de l'erreur. */
	private String identifiant;
	/** La sévérité de l'erreur. */
	private Severite severite;
	/** Le titre de l'erreur. */
	private String titre;
	/** La description de l'erreur. */
	private String description;

	// Éléments sans Javadoc

	/** Getter ou setter sans Javadoc. */
	public final static Erreur GETTER_SETTER_SANS_JAVADOC = new Erreur("getter_setter_sans_javadoc",
			Severite.ERREUR_JAVA,
			"{Getter ou setter} sans commentaire Javadoc", "Un {} doit avoir un commentaire Javadoc débutant par /**.");

	/** Propriété sans Javadoc. */
	public final static Erreur PROPRIETE_SANS_JAVADOC = new Erreur("propriete_sans_javadoc", Severite.ERREUR_JAVA,
			"Propriété sans commentaire Javadoc", "Une propriété doit avoir un commentaire Javadoc débutant par /**.");

	/** Élément sans Javadoc. */
	public final static Erreur ELEMENT_SANS_JAVADOC = new Erreur("element_sans_javadoc", Severite.ERREUR_JAVA,
			"{Autre élément} sans commentaire Javadoc", "Une {} doit avoir un commentaire Javadoc débutant par /**.");

	/** Commentaire Javadoc vide. */
	public final static Erreur JAVADOC_VIDE = new Erreur("javadoc_vide", Severite.ERREUR_JAVA,
			"Commentaire Javadoc vide", "Ce commentaire Javadoc ne contient pas de description.");

	/** Commentaire Javadoc dans le vide. */
	public final static Erreur JAVADOC_FLOTTANTE = new Erreur("javadoc_flottante", Severite.ERREUR_JAVA,
			"Commentaire Javadoc qui ne documente rien",
			"Ce commentaire Javadoc ne documente rien. Les commentaires Javadoc doivent être placés immédiatement avant une déclaration de classe, méthode ou propriété.");

	// Propriétés

	/** Propriété publique. */
	public final static Erreur PROPRIETE_PUBLIQUE = new Erreur("propriete_publique", Severite.ERREUR_JAVA,
			"Propriété publique", "Les propriétés doivent être privées (ou protégées en cas d'héritage).");

	/** Propriété protégée. */
	public final static Erreur PROPRIETE_PROTEGE = new Erreur("propriete_protege", Severite.AVERTISSEMENT,
			"Propriété protégée", "Les propriétés protégées ne peuvent être utilisés qu'en cas d'héritage.");

	/** Propriété avec visibilité par défaut. */
	public final static Erreur PROPRIETE_VISIBILITE_DEFAUT = new Erreur("propriete_visibilite_defaut",
			Severite.NORME_SIM,
			"Propriété avec visibilité par défaut",
			"Les propriétés ne doivent pas être laissées avec leur visibilité par défaut. Rendez-la privée (ou protégée en cas d'héritage).");

	/** Propriété statique. */
	public final static Erreur PROPRIETE_STATIQUE = new Erreur("propriete_statique", Severite.AVERTISSEMENT,
			"Propriété statique", "Est-il essentiel et judicieux d'utiliser une propriété de classe dans ce cas ?");

	// Identificateurs

	/** Identificateur de classe invalide. */
	public final static Erreur IDENTIFICATEUR_CLASSE = new Erreur("identificateur_classe", Severite.ERREUR_JAVA,
			"{Classe} mal nommée",
			"Le nom d'une {} doit commencer par une majuscule et ne peut contenir que des minuscules, des chiffres et des majuscules pour la première lettre des mots.");

	/** Identificateur de constante invalide. */
	public final static Erreur IDENTIFICATEUR_CONSTANTE = new Erreur("identificateur_constante", Severite.ERREUR_JAVA,
			"Constante mal nommée",
			"Le nom d'une constante ne peut contenir que des majuscules, des chiffres et des _ pour séparer les mots.");

	/** Identificateur de variable invalide. */
	public final static Erreur IDENTIFICATEUR_VARIABLE = new Erreur("identificateur_variable", Severite.ERREUR_JAVA,
			"{Variable} mal nommée",
			"Le nom des {} doit commencer par une minuscule et ne peut contenir que des minuscules, des chiffres et des majuscules pour la première lettre des mots.");

	// Revendication de méthodes

	/** Nom à l'intérieur d'un méthode. */
	public final static Erreur NOM_DANS_METHODE = new Erreur("nom_dans_methode", Severite.NORME_SIM,
			"Commentaire de nom dans une méthode",
			"Commentaire de nom mal placé : seule une méthode entière peut être revendiquée.");

	/** Nom qui n'est pas sur une méthode. */
	public final static Erreur NOM_MAL_PLACE = new Erreur("nom_mal_place", Severite.NORME_SIM,
			"Commentaire de nom mal placé",
			"Commentaire de nom mal placé : le commentaire pour revendiquer une méthode doit être placé immédiatement avant sa déclaration (incluant les annotations comme @Override).");

	/** Méthode revendiquée par plusieurs personnes. */
	public final static Erreur METHODE_PLUSIEURS_NOMS = new Erreur("methode_plusieurs_noms", Severite.NORME_SIM,
			"Méthode revendiquée par plusieurs personnes",
			"Une méthode ne peut être revendiquée que par une seule personne. Utilisez des sous-méthodes pour revendiquer différentes parties de la méthode.");

	/** Méthode sans nom. */
	public final static Erreur METHODE_SANS_NOM = new Erreur("methode_sans_nom", Severite.NORME_SIM,
			"Méthode non revendiquée",
			"Ajoutez le nom de la personne qui a codée la méthode avec un commentaire débutant par //.");

	/** Méthode avec un nom qui n'est pas en @author dans la classe. */
	public final static Erreur NOM_PAS_AUTHOR = new Erreur("nom_pas_author", Severite.NORME_SIM,
			"{nom} n'est pas en @author dans la classe",
			"{} a revendiqué cette méthode, mais cette personne n'est pas en @author dans la classe.");

	// Balises @author

	/** Balise @author manquante. */
	public final static Erreur BALISE_AUTHOR_MANQUANTE = new Erreur("balise_author_manquante", Severite.ERREUR_JAVA,
			"Balise @author manquante", "Le nom de l'auteur(e) de la classe doit être ajouté avec la balise @author.");

	/** Personne en @author qui ne l'est pas dans la classe parent. */
	public final static Erreur AUTHOR_PAS_DANS_PARENT = new Erreur("author_pas_dans_parent", Severite.ERREUR_JAVA,
			"Personne pas en @author dans la classe parent",
			"{} est en @author dans cette classe, mais pas dans la classe parent.");

	/** Personne en @author qui n'a aucune méthode */
	public final static Erreur AUTHOR_SANS_METHODES = new Erreur("author_sans_methodes", Severite.ERREUR_JAVA,
			"Personne en @author qui n'a aucune méthode dans la classe",
			"{} est en @author, mais n'a codé aucune méthode dans la classe.");

	/** Plusieurs balises @author sur une énumération. */
	public final static Erreur PLUSIEURS_AUTHOR_ENUM = new Erreur("plusieurs_author_enum", Severite.NORME_SIM,
			"Plusieurs balises @author sur une énumération",
			"Les énumérations ne peuvent être réclamées que par une seule personne.");

	/** Balise @author sur un commentaire de méthode. */
	public final static Erreur AUTHOR_SUR_METHODE = new Erreur("author_sur_methode", Severite.ERREUR_JAVA,
			"Balise @author dans un commentaire de méthode",
			"Balise @author mal placée : celle-ci ne peut apparaître que dans le commentaire d'en-tête d'une classe. Utilisez // Votre Nom pour revendiquer la méthode.");

	/** Balise @author dans un commentaire non Javadoc */
	public final static Erreur AUTHOR_NON_JAVADOC = new Erreur("author_non_javadoc", Severite.NORME_SIM,
			"Balise @author dans un commentaire non Javadoc",
			"Balise @author mal placée : celle-ci ne peut apparaître que dans le commentaire d'en-tête d'une classe.");

	// Paramètres de type

	/** Paramètre de type sans commentaire. */
	public final static Erreur PARAMETRE_TYPE_SANS_COMMENTAIRE = new Erreur("parametre_type_sans_commentaire",
			Severite.ERREUR_JAVA, "Paramètre de type sans commentaire",
			"La balise @param <{}> doit être utilisée pour décrire le paramètre de type.");

	/** Paramètre de type inexistant. */
	public final static Erreur PARAMETRE_TYPE_INEXISTANT = new Erreur("parametre_type_inexistant", Severite.ERREUR_JAVA,
			"Paramètre de type inexistant", "Le paramètre de type <{}> n'existe pas.");

	// Méthodes

	/** Méthode avec visibilité par défaut. */
	public final static Erreur METHODE_VISIBILITE_DEFAUT = new Erreur("methode_visibilite_defaut", Severite.NORME_SIM,
			"Méthode avec visibilité par défaut",
			"Les méthodes ne doivent pas être laissées avec leur visibilité par défaut. Précisez public ou private (ou protected en cas d'héritage).");

	/** {@inheritDoc} sans @Override. */
	public final static Erreur INHERITDOC_SANS_OVERRIDE = new Erreur("inheritdoc_sans_override", Severite.ERREUR_JAVA,
			"{@inheritDoc} est utilisé sans l'annotation @Override",
			"Si {@inheritDoc} est utilisé, l'annotation @Override devrait être présente.");

	/** Balise @return sur une méthode de type void. */
	public final static Erreur BALISE_RETURN_VOID = new Erreur("balise_return_void", Severite.ERREUR_JAVA,
			"Balise @return utilisée avec une méthode de type void",
			"La balise @return ne doit pas être utilisée avec une méthode qui ne retourne rien.");

	/** Balise @return sur un constructeur. */
	public final static Erreur BALISE_RETURN_CONSTRUCTEUR = new Erreur("balise_return_constructeur",
			Severite.ERREUR_JAVA,
			"Balise @return utilisée avec un un constructeur",
			"La balise @return ne doit pas être utilisée avec un constructeur.");

	/** Balise @return manquante. */
	public final static Erreur RETURN_MANQUANT = new Erreur("return_manquant", Severite.ERREUR_JAVA,
			"Balise @return manquante",
			"La balise @return doit être utilisée pour décrire la valeur de retour de la méthode.");

	/** Paramètre sans commentaire. */
	public final static Erreur PARAMETRE_SANS_COMMENTAIRE = new Erreur("parametre_sans_commentaire",
			Severite.ERREUR_JAVA,
			"Paramètre sans commentaire", "La balise @param {} doit être utilisée pour décrire le paramètre.");

	/** Exception sans commentaire. */
	public final static Erreur EXCEPTION_SANS_COMMENTAIRE = new Erreur("exception_sans_commentaire",
			Severite.ERREUR_JAVA,
			"Exception sans commentaire", "La balise @throws {} doit être utilisée pour décrire l'exception.");

	/** Paramètre inexistant. */
	public final static Erreur PARAMETRE_INEXISTANT = new Erreur("parametre_inexistant", Severite.ERREUR_JAVA,
			"Paramètre inexistant", "Le paramètre {} n'existe pas.");

	/** Exception inexistante. */
	public final static Erreur EXCEPTION_INEXISTANTE = new Erreur("exception_inexistante", Severite.ERREUR_JAVA,
			"Paramètre inexistant", "L'exception {} n'est pas lancée.");

	// Autre

	/** Annotation @SuppressWarnings. */
	public final static Erreur SUPPRESS_WARNINGS = new Erreur("suppress_warnings", Severite.NORME_SIM,
			"Annotation @SuppressWarnings interdite", "L'annotation @SuppressWarnings ne doit pas être utilisée.");

	/** Marque de conflit Git. */
	public final static Erreur CONFLIT_GIT = new Erreur("conflit_git", Severite.ERREUR_JAVA, "Marque de conflit Git",
			"Le commentaire contient une marque de conflit Git ({}).");

	/** Syntaxe invalide ou non supportée. */
	public final static Erreur SYNTAXE_INVALIDE = new Erreur("syntaxe_invalide", Severite.ERREUR_JAVA,
			"Syntaxe non supportée : le fichier n'a pas été vérifié.", "");

	/** Échec de l'analyse. */
	public final static Erreur ECHEC_ANALYSE = new Erreur("echec_analyse", Severite.AVERTISSEMENT,
			"Échec de l'analyse d'{un élément}", "L'analyse de cet élément a échoué. Soyez vigilants !");

	// Erreurs

	/**
	 * Crée une nouvelle erreur.
	 * 
	 * @param identifiant l'identifiant de l'erreur.
	 * @param severite    la sévérité de l'erreur.
	 * @param titre       le titre de l'erreur.
	 * @param description la description de l'erreur.
	 * @param ajouter     si l'erreur doit être ajoutée à la liste des erreurs.
	 */
	private Erreur(String identifiant, Severite severite, String titre, String description, boolean ajouter) {
		this.identifiant = identifiant;
		this.severite = severite;
		this.titre = titre;
		this.description = description;
		if (ajouter) {
			erreurs.add(this);
		}
	}

	/**
	 * Crée une nouvelle erreur.
	 * 
	 * @param identifiant l'identifiant de l'erreur.
	 * @param severite    la sévérité de l'erreur.
	 * @param titre       le titre de l'erreur.
	 * @param description la description de l'erreur.
	 */
	private Erreur(String identifiant, Severite severite, String titre, String description) {
		this(identifiant, severite, titre, description, true);
	}

	/**
	 * Retourne l'ensemble des erreurs.
	 * 
	 * @return l'ensemble des erreurs.
	 */
	public static List<Erreur> getErreurs() {
		return erreurs;
	}

	/**
	 * Retourne la sévérité de l'erreur.
	 * 
	 * @return la sévérité de l'erreur.
	 */
	public Severite getSeverite() {
		return severite;
	}

	/**
	 * Retourne le titre de l'erreur.
	 * 
	 * @return le titre de l'erreur.
	 */
	public String getTitre() {
		String titre = this.titre.replaceAll("[\\{\\}]", "");
		return titre.substring(0, 1).toUpperCase() + titre.substring(1);
	}

	/**
	 * Retourne la description de l'erreur.
	 * 
	 * @return la description de l'erreur.
	 */
	public String getDescription() {
		String description = "";
		String ligne = "";
		for (String mot : this.description.split(" ")) {
			if (ligne.length() + mot.length() > 80) {
				description += ligne.trim() + "<br>";
				ligne = "";
			}
			ligne += mot + " ";
		}
		description += ligne.trim();
		return "<html>" + description + "</html>";
	}

	/**
	 * Définit si l'erreur est active.
	 * 
	 * @param active si l'erreur est active.
	 */
	public void setActive(boolean active) {
		preferences.putBoolean(identifiant, active);
	}

	/**
	 * Retourne vrai si l'erreur est active.
	 * 
	 * @return vrai si l'erreur est active.
	 */
	public boolean isActive() {
		return preferences.getBoolean(identifiant, true);
	}

	/**
	 * Retourne cette erreur avec un paramètre.
	 * 
	 * @param parametre le paramètre.
	 * @return cette erreur avec un paramètre.
	 */
	public Erreur avecParametre(String parametre) {
		return new Erreur(identifiant, severite, titre.replaceAll("\\{.*?\\}", parametre),
				description.replaceAll("\\{.*?\\}", parametre), false);
	}

}
