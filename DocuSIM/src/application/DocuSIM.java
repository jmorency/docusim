/* Docusim, outil de vérification de la Javadoc et des normes en SIM.
   Copyright (C) 2023 Jean-Félix Morency

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

package application;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.prefs.Preferences;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.github.javaparser.Range;

import composants.InterfaceSplitPane;
import composants.arbre.AfficheurCelluleErreur;
import composants.arbre.AfficheurCelluleFichier;
import composants.visualisateur.VisualisateurJava;
import verification.Erreur;
import verification.Verification;

/**
 * Application qui permet de vérifier les normes de Javadoc de SIM.
 * 
 * @author Jean-Félix Morency
 */
public class DocuSIM extends JFrame {

	/** Identifiant de sérialisation. */
	private static final long serialVersionUID = 1L;

	/** Les préférences de l'utilisateur. */
	private Preferences preferences = Preferences.userRoot().node("docusim");
	/** Le projet ouvert. */
	private File projet;
	/** Les noms trouvés dans le projet. */
	private Set<String> noms;
	/** Les problèmes dans le projet. */
	private List<Object[]> problemes;

	/** L'item de menu pour actualiser le projet. */
	private JMenuItem mntmActualiser;
	/** L'arbre des fichiers. */
	private JTree treeFichiers;
	/** L'arbre des problèmes. */
	private JTree treeProblemes;
	/** Le panneau à gauche. */
	private JPanel panelGauche;
	/** L'item de menu pour ouvrir un projet. */
	private JMenuItem mntmOuvrir;
	/** Le panneau de visualisation de code Java. */
	private VisualisateurJava visualisateurJava;
	/** Le panneau des noms trouvés dans le projet. */
	private JPanel panelNomPersonnes;

	/**
	 * Démarre l'application.
	 * 
	 * @param args paramètres d'entrée de la commande de démarrage.
	 */
	public static void main(String[] args) {
		System.setProperty("apple.laf.useScreenMenuBar", "true");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DocuSIM frame = new DocuSIM();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Crée la fenêtre.
	 */
	public DocuSIM() {
		setTitle("DocuSIM");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(preferences.getInt("x", 100), preferences.getInt("y", 100), preferences.getInt("width", 450),
				preferences.getInt("height", 300));
		setExtendedState(preferences.getInt("window_state", 0));

		JMenuBar menuBar = new JMenuBar();
		Theme.ajouterComposant(menuBar);
		menuBar.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(128, 128, 128)));
		setJMenuBar(menuBar);

		JMenu mnFichier = new JMenu("Fichier");
		Theme.ajouterComposant(mnFichier);
		menuBar.add(mnFichier);

		mntmOuvrir = new JMenuItem("Ouvrir un projet");
		mntmOuvrir.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()));
		mntmOuvrir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ouvrirProjet();
			}
		});
		mnFichier.add(mntmOuvrir);

		mntmActualiser = new JMenuItem("Actualiser");
		mntmActualiser.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		mntmActualiser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actualiser(false);
			}
		});
		mntmActualiser.setEnabled(false);
		mnFichier.add(mntmActualiser);

		JMenu mnAffichage = new JMenu("Affichage");
		Theme.ajouterComposant(mnAffichage);
		menuBar.add(mnAffichage);

		JCheckBoxMenuItem chckbxmntmThemeSombre = new JCheckBoxMenuItem("Thème sombre");
		chckbxmntmThemeSombre.setSelected(Theme.isSombre());
		chckbxmntmThemeSombre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Theme.setSombre(!Theme.isSombre());
			}
		});

		JMenuItem mntmSelectionnerErreurs = new JMenuItem("Sélection des erreurs");
		mntmSelectionnerErreurs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SelectionErreurs selectionErreurs = new SelectionErreurs(DocuSIM.this);
				selectionErreurs.setLocationRelativeTo(DocuSIM.this);
				selectionErreurs.setVisible(true);
				if (projet != null) {
					visualisateurJava.actualiserErreurs();
					genererArbreProblemes();
					repaint();
				}
			}
		});
		mnAffichage.add(mntmSelectionnerErreurs);
		mnAffichage.add(chckbxmntmThemeSombre);

		JMenu mnAide = new JMenu("Aide");
		Theme.ajouterComposant(mnAide);
		menuBar.add(mnAide);

		JMenuItem mntmAPropos = new JMenuItem("À propos");
		mntmAPropos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FenetreDialogue.afficherAPropos(DocuSIM.this);
			}
		});
		mnAide.add(mntmAPropos);

		JMenuItem mntmInstructions = new JMenuItem("Instructions");
		mntmInstructions.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		mntmInstructions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FenetreDialogue.afficherInstructions(DocuSIM.this);
			}
		});
		mnAide.add(mntmInstructions);

		JMenuItem mntmDon = new JMenuItem("Faire un don");
		mntmDon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().browse(new URI("https://paypal.me/jfmorency"));
				} catch (IOException | URISyntaxException e1) {
					e1.printStackTrace();
				}
			}
		});
		mnAide.add(mntmDon);

		JMenuItem mntmLicense = new JMenuItem("License");
		mntmLicense.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FenetreDialogue.afficherGPL(DocuSIM.this);
			}
		});
		mnAide.add(mntmLicense);

		JPanel contentPane = new JPanel();
		Theme.ajouterPanneau(contentPane);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JSplitPane splitPaneHorizontal = new JSplitPane();
		Theme.ajouterComposant(splitPaneHorizontal);
		splitPaneHorizontal.setDividerSize(5);
		splitPaneHorizontal.setUI(new InterfaceSplitPane());
		splitPaneHorizontal.setBorder(null);
		splitPaneHorizontal.setResizeWeight(0.05);
		contentPane.add(splitPaneHorizontal, BorderLayout.CENTER);

		JSplitPane splitPaneVertical = new JSplitPane();
		splitPaneVertical.setDividerSize(5);
		splitPaneVertical.setUI(new InterfaceSplitPane());
		splitPaneVertical.setBorder(null);
		splitPaneVertical.setOpaque(false);
		splitPaneVertical.setResizeWeight(0.75);
		splitPaneVertical.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPaneHorizontal.setRightComponent(splitPaneVertical);

		visualisateurJava = new VisualisateurJava();
		visualisateurJava.setBorder(new LineBorder(Color.GRAY));
		splitPaneVertical.setLeftComponent(visualisateurJava);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(new LineBorder(Color.GRAY));
		splitPaneVertical.setRightComponent(scrollPane);

		treeProblemes = new JTree();
		Theme.ajouterComposant(treeProblemes);
		treeProblemes.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeProblemes.getLastSelectedPathComponent();
				if (node == null)
					return;
				Object[] erreur = (Object[]) node.getUserObject();
				treeFichiers.setSelectionPath(new TreePath(((DefaultMutableTreeNode) erreur[0]).getPath()));
				if (erreur.length > 2) {
					EventQueue.invokeLater(new Runnable() {
						@Override
						public void run() {
							visualisateurJava.defilerALigne(((Range) erreur[2]).begin.line);
						}
					});
				}
			}
		});
		treeProblemes.setCellRenderer(new AfficheurCelluleErreur());
		treeProblemes.setModel(null);
		scrollPane.setViewportView(treeProblemes);
		treeProblemes.setBorder(new EmptyBorder(5, 5, 5, 5));
		treeProblemes.setRootVisible(false);

		panelGauche = new JPanel();
		panelGauche.setBorder(null);
		panelGauche.setOpaque(false);
		panelGauche.setMinimumSize(new Dimension(200, 10));
		splitPaneHorizontal.setLeftComponent(panelGauche);
		panelGauche.setLayout(new CardLayout(0, 0));

		JPanel panelOuvrirProjet = new JPanel();
		panelOuvrirProjet
				.setBorder(new CompoundBorder(new LineBorder(new Color(128, 128, 128)), new EmptyBorder(5, 5, 5, 5)));
		panelOuvrirProjet.setOpaque(false);
		panelGauche.add(panelOuvrirProjet, "panelOuvrirProjet");
		panelOuvrirProjet.setLayout(new BorderLayout(0, 0));

		JButton btnOuvrirProjet = new JButton("Ouvrir un projet");
		btnOuvrirProjet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ouvrirProjet();
			}
		});
		panelOuvrirProjet.add(btnOuvrirProjet, BorderLayout.NORTH);

		JPanel panelChargement = new JPanel();
		panelChargement
				.setBorder(new CompoundBorder(new LineBorder(new Color(128, 128, 128)), new EmptyBorder(5, 5, 5, 5)));
		panelChargement.setOpaque(false);
		panelGauche.add(panelChargement, "panelChargement");

		JLabel lblChargement = new JLabel("Chargement...");
		Theme.ajouterComposant(lblChargement);
		panelChargement.add(lblChargement);

		JSplitPane splitPaneFichiers = new JSplitPane();
		splitPaneFichiers.setDividerSize(5);
		splitPaneFichiers.setUI(new InterfaceSplitPane());
		splitPaneFichiers.setBorder(null);
		splitPaneFichiers.setResizeWeight(0.8);
		splitPaneFichiers.setOrientation(JSplitPane.VERTICAL_SPLIT);
		panelGauche.add(splitPaneFichiers, "panelFichiers");

		JScrollPane panelFichiers = new JScrollPane();
		splitPaneFichiers.setLeftComponent(panelFichiers);
		panelFichiers.setBorder(new LineBorder(new Color(128, 128, 128)));

		treeFichiers = new JTree();
		treeFichiers.setBorder(new EmptyBorder(5, 5, 5, 5));
		Theme.ajouterComposant(treeFichiers);
		treeFichiers.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeFichiers.getLastSelectedPathComponent();
				if (node != null && node.getUserObject() instanceof Verification) {
					visualisateurJava.setFichier((Verification) node.getUserObject());
				} else {
					visualisateurJava.setFichier(null);
				}
			}
		});
		panelFichiers.setViewportView(treeFichiers);
		treeFichiers.setModel(new DefaultTreeModel(new DefaultMutableTreeNode()));
		treeFichiers.setCellRenderer(new AfficheurCelluleFichier());
		treeFichiers.setForeground(Color.RED);

		JScrollPane panelPersonnes = new JScrollPane();
		splitPaneFichiers.setRightComponent(panelPersonnes);
		panelPersonnes.setOpaque(false);
		panelPersonnes.setBorder(new LineBorder(new Color(128, 128, 128)));

		JLabel lblPersonnes = new JLabel("Personnes trouvées :");
		lblPersonnes.setBorder(new EmptyBorder(5, 5, 0, 5));
		lblPersonnes.setOpaque(true);
		Theme.ajouterComposant(lblPersonnes);
		panelPersonnes.setColumnHeaderView(lblPersonnes);

		panelNomPersonnes = new JPanel();
		Theme.ajouterComposant(panelNomPersonnes);
		panelNomPersonnes.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelPersonnes.setViewportView(panelNomPersonnes);
		panelNomPersonnes.setLayout(new BoxLayout(panelNomPersonnes, BoxLayout.Y_AXIS));

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				ouvrirDernierProjet();
			}
		});

		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				super.componentResized(e);
				preferences.putInt("width", getWidth());
				preferences.putInt("height", getHeight());
			}

			@Override
			public void componentMoved(ComponentEvent e) {
				super.componentMoved(e);
				preferences.putInt("x", getX());
				preferences.putInt("y", getY());
			}
		});
		
		addWindowStateListener(new WindowStateListener() {
			@Override
			public void windowStateChanged(WindowEvent e) {
				preferences.putInt("window_state", getExtendedState());
			}
		});

	}

	/**
	 * Ouvre le dernier projet ouvert par l'utilisateur.
	 */
	private void ouvrirDernierProjet() {
		String dernierProjet = preferences.get("dernier_projet", null);
		if (dernierProjet != null) {
			projet = new File(dernierProjet);
			actualiser(true);
		}
	}

	/**
	 * Demande à l'utilisateur de choisir un projet et l'ouvre.
	 */
	private void ouvrirProjet() {
		JFileChooser selecteur = new JFileChooser(projet);
		selecteur.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		selecteur.showOpenDialog(this);
		if (selecteur.getSelectedFile() != null) {
			projet = selecteur.getSelectedFile();
			preferences.put("dernier_projet", projet.getAbsolutePath());
			actualiser(true);
		}
	}

	/**
	 * Restaure l'arbre des fichiers à un état précédent.
	 * 
	 * @param chemins les chemins à ouvrir.
	 * @param fichier le fichier à ouvrir.
	 */
	private void restaurerArbre(Enumeration<TreePath> chemins, Verification fichier) {
		if (chemins == null)
			return;

		Set<List<Object>> aOuvrir = new HashSet<>();
		while (chemins.hasMoreElements()) {
			TreePath chemin = chemins.nextElement();
			DefaultMutableTreeNode noeud = (DefaultMutableTreeNode) chemin.getLastPathComponent();
			aOuvrir.add(Arrays.asList(noeud.getUserObjectPath()));
		}

		Stack<DefaultMutableTreeNode> aVerifier = new Stack<>();
		aVerifier.push((DefaultMutableTreeNode) treeFichiers.getModel().getRoot());
		while (!aVerifier.empty()) {
			DefaultMutableTreeNode noeud = aVerifier.pop();
			if (aOuvrir.contains(Arrays.asList(noeud.getUserObjectPath()))) {
				treeFichiers.expandPath(new TreePath(noeud.getPath()));
			}
			if (noeud.getUserObject().equals(fichier)) {
				treeFichiers.setSelectionPath(new TreePath(noeud.getPath()));
			}
			for (int i = 0; i < noeud.getChildCount(); i++) {
				aVerifier.push((DefaultMutableTreeNode) noeud.getChildAt(i));
			}
		}
	}

	/**
	 * Actualise le projet.
	 * 
	 * @param nouveauProjet s'il s'agit d'un nouveau projet.
	 */
	private void actualiser(boolean nouveauProjet) {
		int positionCurseur = visualisateurJava.getCaretPosition();
		Verification fichierOuvert = visualisateurJava.getFichier();
		Enumeration<TreePath> cheminsOuverts = treeFichiers
				.getExpandedDescendants(new TreePath(treeFichiers.getModel().getRoot()));

		CardLayout layout = (CardLayout) (panelGauche.getLayout());
		mntmOuvrir.setEnabled(false);
		mntmActualiser.setEnabled(false);
		visualisateurJava.setFichier(null);
		treeProblemes.setModel(null);
		layout.show(panelGauche, "panelChargement");

		problemes = new ArrayList<>();
		noms = new HashSet<>();
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					treeFichiers.setModel(new DefaultTreeModel(ouvrirDossier(projet)));
					genererArbreProblemes();
					if (!nouveauProjet) {
						restaurerArbre(cheminsOuverts, fichierOuvert);
						try {
							visualisateurJava.setCaretPosition(positionCurseur);
						} catch (Exception e) {
							visualisateurJava.setCaretPosition(0);
						}
					}
					panelNomPersonnes.removeAll();
					noms.forEach(nom -> {
						JLabel lblNom = new JLabel("• " + nom);
						lblNom.setFont(new Font("Tahoma", Font.PLAIN, 11));
						Theme.ajouterComposant(lblNom);
						panelNomPersonnes.add(lblNom);
					});

					mntmActualiser.setEnabled(true);
					layout.show(panelGauche, "panelFichiers");
				} catch (Exception e) {
					e.printStackTrace();
					projet = null;
					JOptionPane.showMessageDialog(DocuSIM.this,
							"Une erreur est survenur lors de l'ouverture du projet.", "Erreur",
							JOptionPane.ERROR_MESSAGE);
					layout.show(panelGauche, "panelOuvrirProjet");
				}
				mntmOuvrir.setEnabled(true);
			}
		});
	}

	/**
	 * Ouvre un dossier.
	 * 
	 * @param dossier le dossier.
	 * @return le noeud du dossier.
	 * @throws IOException si une erreur survient en ouvrant le dossier.
	 */
	private DefaultMutableTreeNode ouvrirDossier(File dossier) throws IOException {
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(dossier.getName());
		List<DefaultMutableTreeNode> dossiers = new ArrayList<>();
		List<DefaultMutableTreeNode> fichiers = new ArrayList<>();

		for (File contenu : dossier.listFiles()) {
			if (contenu.isDirectory()) {
				dossiers.add(ouvrirDossier(contenu));
			} else if (contenu.getName().endsWith(".java")) {
				Verification verification = new Verification(contenu);
				noms.addAll(verification.getAuteurs());
				DefaultMutableTreeNode noeudVerification = new DefaultMutableTreeNode(verification);
				fichiers.add(noeudVerification);

				List<Object[]> problemes = new ArrayList<>();
				verification.getProblemes().forEach(probleme -> {
					probleme.getErreurs().forEach(erreur -> {
						problemes.add(new Object[] { noeudVerification, erreur, probleme.getPlage() });
					});
				});
				problemes.sort((a, b) -> ((Range) a[2]).begin.compareTo(((Range) b[2]).begin));
				this.problemes.addAll(problemes);
			}
		}

		for (DefaultMutableTreeNode nodeDossier : dossiers) {
			node.add(nodeDossier);
		}
		for (DefaultMutableTreeNode nodeFichier : fichiers) {
			node.add(nodeFichier);
		}

		return node;
	}

	/**
	 * Génère l'arbre des problèmes.
	 */
	private void genererArbreProblemes() {
		DefaultMutableTreeNode racine = new DefaultMutableTreeNode();
		DefaultMutableTreeNode noeudFichier = new DefaultMutableTreeNode(new Object[4]);

		for (Object[] probleme : problemes) {
			if (((Erreur) probleme[1]).isActive()) {
				DefaultMutableTreeNode fichier = (DefaultMutableTreeNode) probleme[0];
				if (!fichier.equals(((Object[]) noeudFichier.getUserObject())[0])) {
					String nomFichier = ((Verification) fichier.getUserObject()).getFichier().getName();
					noeudFichier = new DefaultMutableTreeNode(new Object[] { fichier, nomFichier });
					racine.add(noeudFichier);
				}
				noeudFichier.add(new DefaultMutableTreeNode(probleme));
			}
		}

		treeProblemes.setModel(new DefaultTreeModel(racine));
		for (int i = 0; i < treeProblemes.getRowCount(); i++) {
			treeProblemes.expandRow(i);
		}
	}

}
