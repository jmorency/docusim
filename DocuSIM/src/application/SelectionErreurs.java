package application;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import verification.Erreur;

/**
 * Boîte de dialogue permettant de sélectionner les erreurs à afficher.
 * 
 * @author Jean-Félix Morency
 */
public class SelectionErreurs extends JDialog {

	/** Identifiant de sérialisation. */
	private static final long serialVersionUID = 1L;

	/**
	 * Crée la boîte de dialogue.
	 * 
	 * @param parent la fenêtre parent.
	 */
	public SelectionErreurs(Frame parent) {
		super(parent);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setTitle("Sélection des erreurs");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().setBackground(Theme.getCouleurPanneau());

		JScrollPane contentPanel = new JScrollPane();
		contentPanel.setOpaque(false);
		contentPanel.setBorder(null);
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel panelErreurs = new JPanel();
			panelErreurs.setBorder(new EmptyBorder(5, 5, 5, 5));
			panelErreurs.setBackground(Theme.getCouleurPanneau());
			contentPanel.setViewportView(panelErreurs);
			panelErreurs.setLayout(new BoxLayout(panelErreurs, BoxLayout.Y_AXIS));

			for (Erreur erreur : Erreur.getErreurs()) {
				JCheckBox checkBox = new JCheckBox(erreur.getTitre(), erreur.isActive());
				checkBox.setForeground(Theme.getCouleurErreur(erreur.getSeverite()));
				checkBox.setOpaque(false);
				checkBox.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						erreur.setActive(checkBox.isSelected());
					}
				});
				panelErreurs.add(checkBox);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setOpaque(false);
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Fermer");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}

}
