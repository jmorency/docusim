# [Télécharger DocuSIM.jar](https://gitlab.com/jmorency/docusim/-/jobs/artifacts/main/raw/DocuSIM.jar?job=build)

# DocuSIM

DocuSIM est une application qui analyse le code source d'un projet Java et signale diverses erreurs de documentation et de normes, basées sur les consignes établies pour les cours 420-SCC et 420-SCD. Merci de [faire un don](https://www.paypal.me/jfmorency).

## Problèmes détectés

- Mauvais identificateurs sur une classe, interface, énumération, propriété, méthode ou variable
- Entités sans Javadoc ou avec une description vide
- Commentaires Javadoc dans le vide
- Annotations `@SuppressWarnings`
- Marques de conflits Git

### Classes

- Propriétés publiques
- Propriétés avec visibilité par défaut
- Balise `@author` manquante
- Personne en `@author` qui n'a pas méthode dans la classe

### Méthodes

- Visibilité par défaut
- Méthode sans nom
- Plusieurs noms sur une méthode
- Nom qui n'est pas en `@author` dans la classe
- Commentaires de noms qui ne sont pas sur une méthode
- `{@inheritDoc}` sans `@Override`
- Balise `@author`

## License

DocuSIM est un logiciel libre distribué sous la license GNU GPLv3. Ce projet utilise la bibliothèque [javaparser](https://github.com/javaparser/javaparser).
